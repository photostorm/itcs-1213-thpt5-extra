/**
 * ObjectArrayFunctionsInterface: This class holds method headers for functions to manipulate arrays with object values
 * 
 * Lab section: L11
 * @author Justin E. Ervin
 * @version 4-19-2014
 */

public interface ObjectArrayFunctionsInterface {
	
	/**
	 * doubleMySize: This method doubles the size of the array and copy the data from the previous array to the new array
	 * 
	 * @param array The array of object values
	 * @return The new array of object values that is double the size of previous array
	 */
	public Object[] doubleMySize(Object[] array);

	/**
	 * getMax: This method finds the largest value in the array
	 * 
	 * @param array The array of object values
	 * @return The max value in the array
	 */
	public Object getMax(Object[] array);

	/**
	 * getMin: This method finds the smallest value in the array
	 * 
	 * @param array The array of object values
	 * @return The min value in the array
	 */
	public Object getMin(Object[] array);
	
	/**
	 * printMe: This method print all the elements in the inputed array
	 * 
	 * @param array The array of object values
	 */
	public void printMe(Object[] array);

	/**
	 * reverseMe: This method reverses all elements in the inputed array
	 * 
	 * @param array The array of object values
	 * @return The new array of object values that is in reverse order
	 */
	public Object[] reverseMe(Object[] array);

	/**
	 * sortMe: This method sorts all the elements in the array using Selection Sort
	 * 
	 * @param array The array of object values
	 * @return The new array of object values in order
	 */
	public Object[] sortMe(Object[] array);

	/**
	 * whereAmI: This method finds the element in the array than equals the search value. If the search value is
	 * not found, then returns negative one.
	 * 
	 * @param array The array of object values
	 * @param searchValue The search value we are looking for
	 * @return subscript of the search value
	 */
	public int whereAmI(Object[] array, Object searchValue);
}
