/**
 * ObjectArrayFunctions: This class holds functions to manipulate arrays with object values
 * 
 * Lab section: L11
 * @author Justin E. Ervin
 * @version 4-19-2014
 */

public class ObjectArrayFunctions implements ObjectArrayFunctionsInterface {
	/**
	 * doubleMySize: This method doubles the size of the array and copy the data from the previous array to the new array
	 * 
	 * @param array The array of object values
	 * @return The new array of object values that is double the size of previous array
	 */
	public Object[] doubleMySize(Object[] array) {
		// Declaring and creating the new array
		Object[] newArray = new Object[array.length * 2];

		// Set all the values of the previous array to the new array
		for (int index = 0; index < array.length; index++) {
			newArray[index] = array[index];
		}

		// Return the new array of object values that is double the size of previous array
		return newArray;
	}

	/**
	 * getMax: This method finds the largest value in the array
	 * 
	 * @param array The array of object values
	 * @return The max value in the array
	 */
	public Object getMax(Object[] array) {
		Object maxValue = null; // Holds the max value

		for (int index = 0; index < array.length; index++) {
			if (index == 0) {
				// Set the starting value
				maxValue = array[index];
			} else if (array[index] instanceof Byte && maxValue instanceof Byte) {
				// If the object is instance of Byte then look for the largest value as byte value in the array
				if (Byte.compare((Byte) array[index], (Byte) maxValue) > 0) {
					maxValue = array[index];
				}
			} else if (array[index] instanceof Short && maxValue instanceof Short) {
				// If the object is instance of Short then look for the largest value as short value in the array
				if (Short.compare((Short) array[index], (Short) maxValue) > 0) {
					maxValue = array[index];
				}
			} else if (array[index] instanceof Integer && maxValue instanceof Integer) {
				// If the object is instance of Integer then look for the largest value as integer value in the array
				if (Integer.compare((Integer) array[index], (Integer) maxValue) > 0) {
					maxValue = array[index];
				}
			} else if (array[index] instanceof Float && maxValue instanceof Float) {
				// If the object is instance of Float then look for the largest value as float value in the array
				if (Float.compare((Float) array[index], (Float) maxValue) > 0) {
					maxValue = array[index];
				}
			} else if (array[index] instanceof Double && maxValue instanceof Double) {
				// If the object is instance of Double then looking the largest value as double value in the array
				if (Double.compare((Double) array[index], (Double) maxValue) > 0) {
					maxValue = array[index];
				}
			} else {
				// If the object is instance of Object then compare the values of the toString method for each Object in the array for the largest value
				if (array[index].toString().compareTo(maxValue.toString()) > 0) {
					maxValue = array[index];
				}
			}
		}

		// Return the max value
		return maxValue;
	}

	/**
	 * getMin: This method finds the smallest value in the array
	 * 
	 * @param array The array of object values
	 * @return The min value in the array
	 */
	public Object getMin(Object[] array) {
		Object minValue = null; // Holds the min value

		for (int index = 0; index < array.length; index++) {
			if (index == 0) {
				minValue = array[index];
			} else if (array[index] instanceof Byte && minValue instanceof Byte) {
				// If the object is instance of Byte then looking the smallest value as byte value in the array
				if (Byte.compare((Byte) array[index], (Byte) minValue) < 0) {
					minValue = array[index];
				}
			} else if (array[index] instanceof Short && minValue instanceof Short) {
				// If the object is instance of Short then looking the smallest value as short value in the array
				if (Short.compare((Short) array[index], (Short) minValue) < 0) {
					minValue = array[index];
				}
			} else if (array[index] instanceof Integer && minValue instanceof Integer) {
				// If the object is instance of Integer then looking the smallest value as integer value in the array
				if (Integer.compare((Integer) array[index], (Integer) minValue) < 0) {
					minValue = array[index];
				}
			} else if (array[index] instanceof Float && minValue instanceof Float) {
				// If the object is instance of Float then looking the smallest value as float value in the array
				if (Float.compare((Float) array[index], (Float) minValue) < 0) {
					minValue = array[index];
				}
			} else if (array[index] instanceof Double && minValue instanceof Double) {
				// If the object is instance of Double then looking the smallest value as double value in the array
				if (Double.compare((Double) array[index], (Double) minValue) < 0) {
					minValue = array[index];
				}
			} else {
				// If the object is instance of Object then compare the values of the toString method for each Object in the array for the smallest value
				if (array[index].toString().compareTo(minValue.toString()) < 0) {
					minValue = array[index];
				}
			}
		}

		// Return the min value
		return minValue;
	}

	/**
	 * printMe: This method print all the elements in the inputed array
	 * 
	 * @param array The array of object values
	 */
	public void printMe(Object[] array) {
		// Print all the elements in the array to the screen
		for (int index = 0; index < array.length; index++) {
			System.out.print(array[index] + " ");
		}
	}

	/**
	 * reverseMe: This method reverses all elements in the inputed array
	 * 
	 * @param array The array of object values
	 * @return The new array of object values that is in reverse order
	 */
	public Object[] reverseMe(Object[] array) {
		// Declaring and creating the new array
		Object[] newArray = new Object[array.length];

		// Setting all elements of the current array in reverse order to the new array
		for (int index = 0; index < array.length; index++) {
			newArray[array.length - index - 1] = array[index];
		}

		// Return the new array of object values that is in reverse order
		return newArray;
	}

	/**
	 * sortMe: This method sorts all the elements in the array using Selection Sort
	 * 
	 * @param array The array of object values
	 * @return The new array of object values in order
	 */
	public Object[] sortMe(Object[] array) {
		// Declaring and creating the new array
		Object[] newArray = new Object[array.length];
		int startScan; // Holds the start of the current range
		int arrayIndex; // Holds the current index of array being read through
		int minIndex; // Holds the index of the min value in the array
		Object minValue; // Holds the min value in the array

		// Set all the values of the previous array to the new array
		for (int index = 0; index < array.length; index++) {
			newArray[index] = array[index];
		}

		for (startScan = 0; startScan < (newArray.length - 1); startScan++) {
			// Set the index and value of the start value of the current range
			minIndex = startScan;
			minValue = newArray[startScan];

			for (arrayIndex = startScan + 1; arrayIndex < newArray.length; arrayIndex++) {
				if (newArray[arrayIndex] instanceof Byte && minValue instanceof Byte) {
					// If the object is instance of Byte then looking the smallest value as byte value in the array
					if (Byte.compare((Byte) newArray[arrayIndex], (Byte) minValue) < 0) {
						minValue = newArray[arrayIndex];
						minIndex = arrayIndex;
					}
				} else if (newArray[arrayIndex] instanceof Short && minValue instanceof Short) {
					// If the object is instance of Short then looking the smallest value as short value in the array
					if (Short.compare((Short) newArray[arrayIndex], (Short) minValue) < 0) {
						minValue = newArray[arrayIndex];
						minIndex = arrayIndex;
					}
				} else if (newArray[arrayIndex] instanceof Integer && minValue instanceof Integer) {
					// If the object is instance of Integer then looking the smallest value as integer value in the array
					if (Integer.compare((Integer) newArray[arrayIndex], (Integer) minValue) < 0) {
						minValue = newArray[arrayIndex];
						minIndex = arrayIndex;
					}
				} else if (newArray[arrayIndex] instanceof Float && minValue instanceof Float) {
					// If the object is instance of Float then looking the smallest value as float value in the array
					if (Float.compare((Float) newArray[arrayIndex], (Float) minValue) < 0) {
						minValue = newArray[arrayIndex];
						minIndex = arrayIndex;
					}
				} else if (newArray[arrayIndex] instanceof Double && minValue instanceof Double) {
					// If the object is instance of Double then looking the smallest value as double value in the array
					if (Double.compare((Double) newArray[arrayIndex], (Double) minValue) < 0) {
						minValue = newArray[arrayIndex];
						minIndex = arrayIndex;
					}
				} else {
					// If the object is instance of Object then compare the values of the toString method for each Object in the array for the smallest value
					if (newArray[arrayIndex].toString().compareTo(minValue.toString()) < 0) {
						minValue = newArray[arrayIndex];
						minIndex = arrayIndex;
					}
				}
			}

			// Setting the current min value to it new position
			newArray[minIndex] = newArray[startScan];
			newArray[startScan] = minValue;
		}

		// Return the new array of object values in order
		return newArray;
	}

	/**
	 * whereAmI: This method finds the element in the array than equals the search value. If the search value is
	 * not found, then returns negative one.
	 * 
	 * @param array The array of object values
	 * @param searchValue The search value we are looking for
	 * @return subscript of the search value
	 */
	public int whereAmI(Object[] array, Object searchValue) {
		int position = -1; // Holds the subscript of the search value and the default value is -1
		boolean isFound = false; // Whether or not the search value was found

		for (int index = 0; index < array.length && !isFound; index++) {
			// Looking for element in the array than equals the search value
			if (array[index].equals(searchValue)) {
				// Saved the position of the searchValue if found
				position = index;
				isFound = true;
			}
		}

		// Return the position of the search value
		return position;
	}
}
