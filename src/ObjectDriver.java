/**
 * Driver: This is a test driver for your ObjectArrayFunctions class
 * 
 * Lab section: L11
 * @author Justin E. Ervin
 * @version 4-19-2014
 */
public class ObjectDriver {
	/**
	 * Execution of this program starts in the main( ) method
	 * 
	 * @param Java arguments
	 */
	public static void main(String[] args) {
		// Creating the test arrays
		Double[] testArrayOne = { 6.0, 0.0, 9.0, 45.0, 20.0 };
		String[] testArrayTwo = { "apple", "coconut", "blueberry", "lemon", "cherry" };
		Object[] results;

		// Creating the reference to ObjectArrayFunctions class
		ObjectArrayFunctions oaf = new ObjectArrayFunctions();

		System.out.println("--- Test 1 ---");
		
		// Printing the elements of testArrayOne array
		System.out.println("This is the original array: ");
		oaf.printMe(testArrayOne);

		// Testing the sortMe and printMe function with the elements of testArrayOne array
		System.out.println("\nThis is a test of the sortMe( ) method and the printMe( ) methods. ");
		results = oaf.sortMe(testArrayOne);
		oaf.printMe(results);

		// Testing the getMax function with the elements of testArrayOne array
		System.out.println("\nThis is a test of getMax( )");
		System.out.println("The maximum value in the array is: " + oaf.getMax(testArrayOne));

		// Testing the getMin function with the elements of testArrayOne array
		System.out.println("\nThis is a test of getMin( )");
		System.out.println("The minimum value in the array is: " + oaf.getMin(testArrayOne));

		// Testing the reverseMe function with the elements of testArrayOne array
		System.out.println("\nThis is a test of reverseMe( )");
		results = oaf.reverseMe(testArrayOne);
		oaf.printMe(results);

		// Testing the whereAmI function with the elements of testArrayOne array
		System.out.println("\nThis is a test of whereAmI( )");
		System.out.println("The value 45.0 is at subscript " + oaf.whereAmI(testArrayOne, 45.0));

		// Testing the doubleMySize function with the elements of testArrayOne array
		System.out.println("\nThis is a test of doubleMySize(  )");
		oaf.printMe(oaf.doubleMySize(testArrayOne));

		System.out.println("\nEnd of test for the first array");

		System.out.println("\n--- Test 2 ---");
		// Printing the elements of testArrayTwo array
		System.out.println("This is the original array for the second array: ");
		oaf.printMe(testArrayTwo);

		// Testing the sortMe and printMe function with the elements of testArrayTwo array
		System.out.println("\nThis is a test of the sortMe( ) method and the printMe( ) methods. ");
		results = oaf.sortMe(testArrayTwo);
		oaf.printMe(results);

		// Testing the getMax function with the elements of testArrayTwo array
		System.out.println("\nThis is a test of getMax( )");
		System.out.println("The maximum value in the array is: " + oaf.getMax(testArrayTwo));

		// Testing the getMin function with the elements of testArrayTwo array
		System.out.println("\nThis is a test of getMin( )");
		System.out.println("The minimum value in the array is: " + oaf.getMin(testArrayTwo));

		// Testing the reverseMe function with the elements of testArrayTwo array
		System.out.println("\nThis is a test of reverseMe( )");
		results = oaf.reverseMe(testArrayTwo);
		oaf.printMe(results);

		// Testing the whereAmI function with the elements of testArrayTwo array
		System.out.println("\nThis is a test of whereAmI( )");
		System.out.println("The value coconut is at subscript " + oaf.whereAmI(testArrayTwo, "coconut"));

		// Testing the doubleMySize function with the elements of testArrayTwo array
		System.out.println("\nThis is a test of doubleMySize(  )");
		oaf.printMe(oaf.doubleMySize(testArrayTwo));

		System.out.println("\nEnd of test for the second array");
	}
}
